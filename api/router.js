
const express = require('express')
  , multipart = require('multiparty')
  , bodyParser = require('body-parser')
  , jsonParser = bodyParser.json()
  , formParser = bodyParser.urlencoded({extended:false})
  ;

// ## 생성부
var apiHandler =  express.Router();
module.exports = apiHandler;

// ## 라우팅
apiHandler.use((req, res, next)=>{
  console.log(`api: ${req.method}  - ${req.url}`);
  next();
});

apiHandler.route('/echo')
  .all((req, res, next)=>{
  res.setHeader('Content-Type', 'text/plain');
  res.write( ' you posted: 1 \n');
  res.end(JSON.stringify(req.body));
});

apiHandler.route('/upload')
  .post( formParser   ,(req, res, next)=>{

    console.log( req.body );
    console.log('up');

    res.end(`${JSON.stringify(req.body)}`);
  });
