//## https://beige-haruasm.c9users.io/

// ## 노드 내장 모듈
const http = require('http')
  , https = require('https')
  , url = require('url')
  , fs = require('fs')
  , path = require('path')
  , qs = require('querystring')
  , util = require('util')

//## 미들웨어
  , serveFavicon = require('serve-favicon')
  , express = require('express')
  , compression  = require('compression')
  , session = require('express-session')
  // , multipart = require('multiparty')
  // , bodyParser = require('body-parser')
  , cookieParser = require('cookie-parser')
  ;

//######################
//## 주요모듈 생성
var app = express()
  // , jsonParser = bodyParser.json()
  // , formParser = bodyParser.urlencoded({extened:false})
  ;

//######################
//## environments
var env = {};
  env.MODE=  'development';  //env.ENV = 'production';
  env.HOST = process.env.HOST || "220.81.12.20";
  env.PORT = process.argv[2] || process.env.NODEPORT || 6114 ;


//#########################
//## 미들웨어 모듈객체 생성

//####################
//## middleware

if(env.MODE == 'development'){

  app.use(function(req,res,next){
    console.log( `${req.method } : ${req.url} ` );
    next();
  });
}
else { // env.MODE == 'production'){
    app.use( compression());
}

//####################
//## routing
app
  .use(express.static(path.join(__dirname,'public')))
  // 세션
  // 인증
  .use(serveFavicon(`${__dirname}/public/images/favicon.ico`) )
  //.use() // parse application/json
  .use(cookieParser())
  //.use(bodyParser.urlencoded({ extended: false })) // parse application/x-www-form-urlencoded
  ;

//## api rouitng
app.use('/a', require('./api/router.js'));

app.use('/book/:id', function(req, res, next) {
  console.log('ID:', req.params.id);
  res.end("yea"+ req.params.id );
});



// http listening
const httpServer  = http.createServer(app).listen(env.PORT, ()=>{
    console.log(`http listening on ${env.HOST}:${env.PORT}`);
});

//https.createServer(app).listen(443, ()=>{console.log('443')});
