//## https://beige-haruasm.c9users.io/

// ## 노드 내장 모듈
const http = require('http')
  , url = require('url')
  , fs = require('fs')
  , path = require('path')
  , qs = require('querystring')
  , mp = require('multiparty')
  , util = require('util')
  , express = require('express')
  ;

//######################
//## 주요모듈 생성


//######################
//## environments

var env = {};
  env.ENV=  'development';
     //,env= 'production'
  env.HOST = process.env.HOST || "220.81.12.20";
  env.PORT = process.argv[2] || process.env.NODEPORT || 6114 ;

//#########################
//## 미들웨어 모듈객체 생성

//####################
//## middleware
// dev mode
if(env.ENV == 'development'){

}
// production mode
else if(env.ENV == 'production'){

}

//####################
//## handler 노드 내장 사용
var router = Object.create(null);

  router['/'] = function(req, res){
    res.end('Hello\n\n A server is running.\n\n\n Beans Soft Apps\n');
  }; // '/'

  router['/erp'] = (req, res)=>{
    fs.createReadStream('./private/dashboard.html', {flags: 'r'}).pipe(res, { end: false })
    .on('end', ()=>{ console.log('send dashboard'); res.end();});
  };

  router['/favicon.ico'] = function(req, res){
        res.writeHead(203, {'content-type' : 'application/json'});
        res.end(JSON.stringify({nope:true} ));

  }; // favicon

  router['/api/get'] = function(req, res){

    var filename = path.basename(req.parsedUrl.pathname);
    serveFile( `./public/${filename}`, res );
  }; // images

  router["/api/upload"]= function(req, res) {

    if( req.method !== 'POST' ){
      res.end("don't know");

      return ;
    }

      var formParser = new mp.Form({uploadDir: './temp'  });
          formParser.maxFilesSize = 1024*1024*2*50; // default 2MB * 50 = 100 MB

          formParser.on('close', function() {
            console.log('Upload completed!');
            // 여기는 파일 송신의 완료지점
            // 따라서 res.end(); 불가
          });

        console.log(formParser.maxFilesSize);
        formParser.parse(req, (err, fields, files)=>{
        //  if( files ){

            // 아직 foreach적용 안됨 - 작업 완료후'close' end() 송신으로 이후 처리
            // serveFile 내부에서 end()호출됨 -> 이후 외부로 옮김
            console.log( fields);
            console.log( files);

            // Object.keys(fields).forEach( name =>{
            //     Object.keys( fields[name]).forEach( (prop)=>{
            //       console.log( name, fields[name], fields[name][prop] );
            //     });
            //
            // }); //fields 순회

            Object.keys( files ).forEach((name)=>{

              Object.keys(files[name]).forEach((num)=>{
                let f = files[name][num]; // 각 파일 객체
                serveFile(f.path, res);
              });

            });

            //res.end();

        //  }// if files


          //return ;

          // res.writeHead( 200, {'content-type':'text/plain'});
          // res.write(`${util.inspect({fields: fields, files: files})} uploaded\n`);
          // return res.end();

        }); // formParser.parse

        // console.log( tmp.name , tmp.filename);
        // console.log( postData );

        // var postData = ''; // post 데이터 chunk
        // req.on('data', str => postData += str );
        // req.on('end', ()=>{
        //var parData = JSON.parse(postData);
        // JSON 파싱
        // streamHandler[parData.code](parData.data, (retCode, msg )=>{
        //
        //   res.writeHead(200, {'Content-Type': 'text/plain'});
        //   res.write(JSON.stringify({code:retCode, msg:msg }));
        //   res.end();
        //
        // }); // streamhandler

        // }); // post method

    console.log('up');
  }; // router - api upload







//## stream 루틴
// 라우팅시, steramHandler[ '루틴' ]에 해당되는 function 실행
// '루틴'은  post 데이터에 포함해서 수신, key값 'code'

var streamHandler = Object.create(null);

  //store 루틴
  // post data 구조
  // {code : 루틴명, data : 데이터}

  streamHandler['store'] = (obj, callback ) =>{
    console.log( `data :: ${objs.name}` );

    fs.writeFile(`./public/${objs.name}`, objs.data, (err)=>{
      if(err) {
        callback(-1, "It's some error. don't worry :D ");
        return console.log(err);
      }
      console.log( `write :: ./public/${objs.name} -len: ${objs.data.length}` );
      callback( 1, 'ok');
    });
  };// streamHAndler



//form 데이터 처리 모듈

// var formParser = new mp.Form({uploadDir: './temp'  });
//     formParser.maxFilesSize = 1024*1024*2*50; // default 2MB * 50 = 100 MB
//
//     formParser.on('close', function() {
//       console.log('Upload completed!');
//
//       res.end();
//     });

var handler = (req, res )=> {
  console.log( `${req.method } : ${req.url} ` );

  req.parsedUrl = path.parse( req.url );

  console.log( "===========" );
  console.log( req.parsedUrl.dir );

  var dirname = req.parsedUrl.dir ;//path.dirname(req.parsedUrl.pathname);



  if( router[dirname] ){
    router[dirname](req, res);
  } //router[uri]

  else
    res.end('nop');

  //}//if router -uri
}// handler

function serveFile(filename, res){

  //filename = `./public/${filename}` ;

  console.log( filename );

  if( !fs.existsSync(filename)){
    res.writeHead(404, {'Content-Type':'text/html' });
    return res.end('<head> <meta charset="UTF-8"></head> <body>404 파일 없음</body>');
  }; // file isn't exist

  fs.readFile( filename, 'binary' , (err, file)=>{
    if(err) return res.end(err);

    res.writeHead(200);
    res.write(file, 'binary');
    res.end();
  }); //readFile

}// serveFile

// http listening
const httpServer  = http.createServer(handler).listen(env.PORT, ()=>{
    console.log(`http listening on ${env.HOST}:${env.PORT}`);
});
